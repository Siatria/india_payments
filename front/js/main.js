let paymentsAll = document.querySelector('.payments-page__all')
let paymentsMore = document.querySelector('.payments-page__more')

paymentsMore.addEventListener('click', () => {
    paymentsAll.classList.toggle('toggle-list');
    paymentsMore.style.display = "none";
});